﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Api.Infraestrutura.Contexto
{
    public interface IControleId<T>
    {
        public int AtribuirId(List<T> listaASerConferida);
    }
}
