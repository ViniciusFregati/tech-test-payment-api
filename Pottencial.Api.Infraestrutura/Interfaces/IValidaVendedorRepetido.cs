﻿using Pottencial.Api.Dominio.Entidades;

namespace Pottencial.Api.Infraestrutura.Interfaces
{
    public interface IValidaVendedorRepetido
    {
        public bool VerificaSeVendedorEhRepetido(List<Vendedor> vendedores, Vendedor vendedor);
    }
}
