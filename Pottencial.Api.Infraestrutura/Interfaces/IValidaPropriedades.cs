﻿using Pottencial.Api.Dominio.Entidades;

namespace Pottencial.Api.Infraestrutura.Interfaces
{
    public interface IValidaPropriedades
    {
        public void ValidaCpf(string cpf);
        public void ValidaTelefone(string telefone);
        public void ValidaListaDeCompras(Venda venda);
    }
}
