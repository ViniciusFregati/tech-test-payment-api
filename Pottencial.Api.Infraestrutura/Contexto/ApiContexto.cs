﻿using System.Text.RegularExpressions;
using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Excecoes;
using Pottencial.Api.Dominio.Models;
using Pottencial.Api.Infraestrutura.Interfaces;

namespace Pottencial.Api.Infraestrutura.Contexto
{
    /// <summary>
    /// Classe que faz o controle dos dados que serão persistidos em memória, ela é responsável por adicionar itens e validá-los
    /// </summary>
    public class ApiContexto : IContext, IControleId<Vendedor>, IControleId<Venda>, IValidaPropriedades, IValidaVendedorRepetido
    {
        public List<Vendedor> Vendedores { get; }
        public List<Venda> Vendas { get; }

        public ApiContexto()
        {
            Vendedores = new List<Vendedor>();
            Vendas = new List<Venda>();
        }

        /// <summary>
        /// Método utilizado para salvar uma venda, ele também valida se ela está dentro dos parâmetros.
        /// Pode retornar exceções se: a lista de itens vendidos estiver vazia, o CPF estiver fora do padrão
        /// ou se o número do telefone também estiver fora do padrão.
        /// </summary>
        /// <param name="venda">Venda a ser adicionada.</param>
        /// <exception cref="ListaVaziaException"></exception>
        /// <exception cref="CpfInvalidoException"></exception>
        /// <exception cref="TelefoneInvalidoException"></exception>
        public Venda SalvarVenda(Venda venda)
        {
            ValidaListaDeCompras(venda);
            SalvarVendedor(venda.Vendedor);

            venda.Id = AtribuirId(Vendas);
            venda.VendedorId = venda.Vendedor.Id;
            venda.Data = DateTime.Now.Date;
            venda.StatusVenda = EnumStatusVenda.AguardandoPagamento;

            Vendas.Add(venda);
            return venda;
        }

        /// <summary>
        /// Método utilizado para salvar um vendedor, também é validado se ele está dentro dos parâmetros.
        /// Pode retornar exceções se: o CPF ou o número do telefone estiverem fora do padrão
        /// </summary>
        /// <param name="vendedor">Vendedor a ser adicionado</param>
        /// <exception cref="CpfInvalidoException"></exception>
        /// <exception cref="TelefoneInvalidoException"></exception>
        public Vendedor SalvarVendedor(Vendedor vendedor)
        {
            ValidaCpf(vendedor.Cpf);
            ValidaTelefone(vendedor.Telefone);

            if (!VerificaSeVendedorEhRepetido(Vendedores, vendedor))
            {
                vendedor.Id = AtribuirId(Vendedores);
                Vendedores.Add(vendedor);
            }

            return vendedor;
        }

        /// <summary>
        /// Incrementa o id do vendedor
        /// </summary>
        /// <param name="listaASerConferida">Lista de vendedores a qual se deseja verificar o próximo id.</param>
        /// <returns>Retorna o valor do próximo id a ser utilizado.</returns>
        public int AtribuirId(List<Vendedor> listaASerConferida)
        {
            int id = listaASerConferida.Count == 0 ? 1 
                     : listaASerConferida.Last().Id + 1;

            return id;
        }

        /// <summary>
        /// Incrementa o id da venda.
        /// </summary>
        /// <param name="listaASerConferida">Lista de vendas a qual se deseja verificar o próximo id.</param>
        /// <returns>Retorna o valor do próximo id a ser utilizado</returns>
        public int AtribuirId(List<Venda> listaASerConferida)
        {
            int id = listaASerConferida.Count == 0 ? 1
                     : listaASerConferida.Last().Id + 1;

            return id;
        }

        /// <summary>
        /// Realiza a validação do CPF, caso esteja fora do padrão, retorna uma exceção.
        /// </summary>
        /// <param name="cpf">CPF que se deseja verificar.</param>
        /// <exception cref="CpfInvalidoException"></exception>
        public void ValidaCpf(string cpf)
        {
            Regex numeroCpf = new("^[0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2}$");

            if (!numeroCpf.IsMatch(cpf))
                throw new CpfInvalidoException("CPF inválido, ele deve estar no formato: 000.000.000-00.");
        }

        /// <summary>
        /// Realiza a validação do telefone, caseo esteja fora do padrão, retorna uma exceção.
        /// </summary>
        /// <param name="telefone">Telefone que se deseja verificar.</param>
        /// <exception cref="TelefoneInvalidoException"></exception>
        public void ValidaTelefone(string telefone)
        {
            Regex numeroTelefone = new("^\\([0-9]{2}\\)[0-9]{5}-[0-9]{4}$");

            if (!numeroTelefone.IsMatch(telefone))
                throw new TelefoneInvalidoException("Telefone inválido, ele deve estar no formato: (00)00000-0000.");
        }

        /// <summary>
        /// Valida se a lista de compras possuí no mínimo um item, caso não, lança uma exceção.
        /// </summary>
        /// <param name="venda">Venda a qual se deseja verificar a lista de itens vendidos.</param>
        /// <exception cref="ListaVaziaException"></exception>
        public void ValidaListaDeCompras(Venda venda)
        {
            if (venda.ItensVendidos.Count < 1)
                throw new ListaVaziaException("A lista de itens vendidos deve possuir pelo menos um item.");
        }

        /// <summary>
        /// Verifica se o vendedor que se deseja adicionar já não está cadastrado na lista de vendedores.
        /// </summary>
        /// <param name="vendedores">Lista de vendedores na qual se deseja procurar o vendedor.</param>
        /// <param name="vendedor">Vendedor o qual se deseja verificar se já não está cadastrado.</param>
        /// <returns>Retorna verdadeiro caso o vendedor já exista, falso se ele for um novo.</returns>
        public bool VerificaSeVendedorEhRepetido(List<Vendedor> vendedores, Vendedor vendedor)
        {
            Vendedor? vendedorEncontrado = vendedores.FirstOrDefault(v => v.Cpf == vendedor.Cpf);

            if (vendedorEncontrado != null)
            {
                vendedor.Id = vendedorEncontrado.Id;
                return true;
            }

            return false;
        }
    }
}
