﻿using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Excecoes;
using Pottencial.Api.Infraestrutura.Contexto;

namespace Pottencial.Api.Testes.Infraestrutura
{
    public class ApiContextoTeste
    {
        private readonly ApiContexto _context;
        private readonly Vendedor _vendedor;
        private readonly Venda _venda;

        public ApiContextoTeste()
        {
            _context = new ApiContexto();
            _vendedor = new Vendedor
            {
                Nome = "Vinícius Silva",
                Cpf = "496.412.421-73",
                Email = "vinicius@gmail.com",
                Telefone = "(17)99149-6246"
            };

            _venda = new Venda
            {
                Vendedor = _vendedor,
                ItensVendidos = new List<string>
                {
                    "Curso .NET Básico"
                }
            };
        }

        [Fact]
        public void TestaAdicionarVendedor()
        {
            //Arrange
            int quantidadeInicial = _context.Vendedores.Count;

            //Act
            _context.SalvarVendedor(_vendedor);

            //Assert
            Assert.True(_context.Vendedores.Count > quantidadeInicial);
            Assert.Equal(_vendedor.Nome, _context.Vendedores.Last().Nome);
            Assert.Equal(1, _vendedor.Id);
        }

        [Fact]
        public void TestaAdicionarVenda()
        {
            //Arrange
            int quantidadeInicialVendas = _context.Vendas.Count;

            //Act
            _context.SalvarVenda(_venda);

            //Assert
            Assert.True(_context.Vendas.Count > quantidadeInicialVendas);
            Assert.Equal(_vendedor.Nome, _context.Vendas.Last().Vendedor.Nome);
            Assert.Equal(1, _context.Vendas.Last().Id);
        }

        [Fact]
        public void TestaExcecaoTelefoneInvalido()
        {
            //Arrange
            Vendedor vendedor = new()
            {
                Telefone = "99156-5297",
                Cpf = "465.315.256-78",
                Email = "tales@gmail.com",
                Nome = "Tales Souza"
            };

            //Act
            //Assert
            Assert.Throws<TelefoneInvalidoException>(
                () => _context.SalvarVendedor(vendedor)
            );
        }

        [Fact]
        public void TestaExcecaoCpfInvalido()
        {
            //Arrange
            Vendedor vendedor = new()
            {
                Telefone = "(17)99156-5297",
                Cpf = "465.315.256.78",
                Email = "tales@gmail.com",
                Nome = "Tales Souza"
            };

            //Act
            //Assert
            Assert.Throws<CpfInvalidoException>(
                () => _context.SalvarVendedor(vendedor)
            );
        }

        [Fact]
        public void TestaExcecaoListaVazia()
        {
            //Arrange
            Venda venda = new()
            {
                Vendedor = _vendedor,
                ItensVendidos = new List<string>()
            };

            //Act
            //Assert
            Assert.Throws<ListaVaziaException>(
                () => _context.SalvarVenda(venda)
            );
        }

        [Fact]
        public void TestaAcrescimoDoIdNoVendedor()
        {
            //Arrange
            _context.SalvarVendedor(_vendedor);
            int idPrimeiroVendedor = _context.Vendedores.Last().Id;
            int idSegundoVendedor;

            Vendedor segundoVendedor = new()
            {
                Nome = "Ronaldo Silva",
                Email = "ronaldo@gmail.com",
                Telefone = "(16)99289-6142",
                Cpf = "421.537.723-78"
            };

            //Act
            _context.SalvarVendedor(segundoVendedor);
            idSegundoVendedor = _context.Vendedores.Last().Id;

            //Assert
            Assert.Equal(1, idPrimeiroVendedor);
            Assert.Equal(2, idSegundoVendedor);
        }

        [Fact]
        public void TestaAcrescimoDoIdNaVenda()
        {
            //Arrange
            _context.SalvarVenda(_venda);
            int idPrimeiraVenda = _context.Vendas.Last().Id;
            int idSegundaVenda;

            Venda segundaVenda = new()
            {
                Vendedor = _vendedor,
                ItensVendidos = new List<string> { "Curso .NET Intermediário", "Curso .NET Avançado" }
            };

            //Act
            _context.SalvarVenda(segundaVenda);
            idSegundaVenda = _context.Vendas.Last().Id;

            //Assert
            Assert.Equal(1, idPrimeiraVenda);
            Assert.Equal(2, idSegundaVenda);
        }

        [Fact]
        public void TestaNaoCadastrarVendedorRepetido()
        {
            //Arrange
            _context.SalvarVendedor(_vendedor);
            int quantidadeDeVendedoresCadastrados = _context.Vendedores.Count;

            //Act
            _context.SalvarVendedor(_vendedor);

            //Assert
            Assert.Equal(quantidadeDeVendedoresCadastrados, _context.Vendedores.Count);
        }
    }
}
