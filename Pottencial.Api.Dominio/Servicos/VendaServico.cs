﻿using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Excecoes;
using Pottencial.Api.Dominio.Interfaces.Repositorios;
using Pottencial.Api.Dominio.Interfaces.Servicos;
using Pottencial.Api.Dominio.Models;

namespace Pottencial.Api.Dominio.Servicos
{
    public class VendaServico : IVendaServico
    {
        private readonly IVendaRepositorio _repositorio;

        public VendaServico(IVendaRepositorio repositorio)
        {
            _repositorio = repositorio;
        }

        /// <summary>
        /// Atualiza o status da venda.
        /// </summary>
        /// <param name="id">Id da venda que se deseja atualizar o status.</param>
        /// <param name="novoStatus">Novo status.</param>
        /// <returns>Retorna um booleano dizendo se conseguiu atualizar.</returns>
        /// <exception cref="AtualizacaoStatusInvalidoException"></exception>
        /// <exception cref="NaoEncontradoException"></exception>
        public bool AtualizarStatusVenda(int id, EnumStatusVenda novoStatus)
        {
            return _repositorio.AtualizarStatusVenda(id, novoStatus);
        }

        /// <summary>
        /// Busca uma venda através do seu Id.
        /// </summary>
        /// <param name="id">Id da venda que se deseja buscar.</param>
        /// <returns>Retorna a venda encontrada.</returns>
        /// <exception cref="NaoEncontradoException"></exception>
        public Venda BuscarVendaPeloId(int id)
        {
            return _repositorio.BuscarVendaPeloId(id);
        }

        /// <summary>
        /// Registra uma venda
        /// </summary>
        /// <param name="venda">Venda a qual se deseja registrar.</param>
        /// <returns>Retorna a venda registrada com todas as propriedades atribuidas.</returns>
        /// <exception cref="TelefoneInvalidoException"></exception>
        /// <exception cref="CpfInvalidoException"></exception>
        /// <exception cref="ListaVaziaException"></exception>
        public Venda RegistrarVenda(Venda venda)
        {
            Venda vendaSalva = _repositorio.RegistrarVenda(venda);
            return vendaSalva;
        }
    }
}
