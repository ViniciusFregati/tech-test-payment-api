﻿namespace Pottencial.Api.Dominio.Excecoes
{
    [Serializable]
    public class AtualizacaoStatusInvalidoException : Exception
    {
        public AtualizacaoStatusInvalidoException() { }
        public AtualizacaoStatusInvalidoException(string message) : base(message) { }
        public AtualizacaoStatusInvalidoException(string message, Exception innerException) : base(message, innerException) { }
    }
}
