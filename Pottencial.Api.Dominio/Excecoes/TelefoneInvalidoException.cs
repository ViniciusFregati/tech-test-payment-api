﻿namespace Pottencial.Api.Dominio.Excecoes
{
    [Serializable]
    public class TelefoneInvalidoException : Exception
    {
        public TelefoneInvalidoException() { }
        public TelefoneInvalidoException(string message) : base(message) { }
        public TelefoneInvalidoException(string message, Exception innerException) : base(message, innerException) { }
    }
}
