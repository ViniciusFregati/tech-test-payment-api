﻿namespace Pottencial.Api.Dominio.Excecoes
{
    [Serializable]
    public class CpfInvalidoException : Exception
    {
        public CpfInvalidoException() { }
        public CpfInvalidoException(string message) : base(message) { }
        public CpfInvalidoException(string message, Exception innerException) : base(message, innerException) { }
    }
}
