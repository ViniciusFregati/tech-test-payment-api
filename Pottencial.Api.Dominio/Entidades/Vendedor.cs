﻿using System.ComponentModel.DataAnnotations;

namespace Pottencial.Api.Dominio.Entidades
{
    /// <summary>
    /// Entidade que ilustra um vendedor, contendo suas principais informações.
    /// </summary>
    public class Vendedor
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "O campo cpf é obrigatório")]
        public string Cpf { get; set; }
        [Required(ErrorMessage = "O campo nome é obrigatório")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "O campo e-mail é obrigatório")]
        public string Email { get; set; }
        [Required(ErrorMessage = "O campo telefone é obrigatório")]
        public string Telefone { get; set; }
    }
}
