﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Aplicacao.Interfaces;
using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Excecoes;
using Pottencial.Api.Dominio.Models;

namespace Pottencial.Api.Apresentacao.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class VendaController : ControllerBase
    {
        private readonly IVendaServicoApp _service;
        private readonly IMapper _mapper;

        public VendaController(IVendaServicoApp vendaServicoApp, IMapper mapper)
        {
            _service = vendaServicoApp;
            _mapper = mapper;
        }

        /// <summary>
        /// Recebe uma venda pelo seu Id.
        /// </summary>
        /// <param name="id">Id da venda que deseja receber.</param>
        /// <returns>Retorna status 200 se encontrar a venda, 404 caso não encontre.</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult ObterVendaPorId(int id)
        {
            try
            {
                VendaDTOGet venda = _service.BuscarVendaPeloId(id);
                return Ok(venda);
            }
            catch (NaoEncontradoException ex)
            {
                return NotFound(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// Cadastra uma nova venda.
        /// </summary>
        /// <param name="vendaDtoSet">Venda que se deseja cadastrar.</param>
        /// <returns>Retorna uma IActionResult, sendo 201 se o cadastro ocorrer com sucesso, 
        /// 400 se algum dado for passado de maneira incorreta.</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CadastrarVenda(VendaDTOSet vendaDtoSet)
        {
            try
            {
                Venda vendaSalva = _service.RegistrarVenda(vendaDtoSet);

                return CreatedAtAction(nameof(ObterVendaPorId), new { Id = vendaSalva.Id }, vendaSalva);
            }
            catch (Exception ex) when (ex is CpfInvalidoException || ex is TelefoneInvalidoException 
                                       || ex is ListaVaziaException)
            {
                return BadRequest(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// Atualiza o status da venda.
        /// </summary>
        /// <param name="id">Id da venda que se deseja atualizar</param>
        /// <param name="status">Os possíveis status de venda são: 0 para aguardando pagamento, 
        /// 1 para pagamento aprovado, 2 para enviado para transportadora, 3 para entregue
        /// e 4 para cancelado.</param>
        /// <returns>Retorna um IActionResult que pode ser 204 se tiver sucesso ou 400 caso algum dado seja passado de forma incorreta. 
        /// Pode também ser 404 caso a venda não seja encontrada.</returns>
        [HttpPatch("AtualizarStatusVenda/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult AtualizarStatusVenda(int id, EnumStatusVenda status)
        {
            try
            {
                _service.AtualizarStatusVenda(id, status);
                return NoContent();
            }
            catch (NaoEncontradoException ex)
            {
                return NotFound(new { Error = ex.Message });
            }
            catch (AtualizacaoStatusInvalidoException ex)
            {
                return BadRequest(new { Error = ex.Message });
            }
        }
    }
}
