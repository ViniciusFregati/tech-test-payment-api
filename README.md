# Apresentação do desafio

Projeto final do bootcamp Pottencial .NET Developer, foi requerido a criação de uma API REST, essa deve ter uma rota com documentação swagger e possuir 3 operações, sendo elas: registrar venda (recebe os dados do vendedor, os itens vendidos e atribui o status como "Aguardando pagamento"), buscar venda (busca a venda pelo seu Id) e atualizar venda (atualiza o status da venda).

## Regras do desafio

 - Vendedor deve possuir as informações de seu Id, cpf, nome, e-mail e telefone;
 - Venda deve possuir as informações do vendedor, os itens vendidos, a data, Id e status;
 - Para incluir uma venda, ela deve possuir ao menos um item;
 - Os status possíveis da venda são: `Pagamento aprovado` | `Enviado para transportadora` | `Entregue` | `Cancelada`;
 - As possíveis transições de status são:
  - De: `Aguardando pagamento` Para: `Pagamento Aprovado`
  - De: `Aguardando pagamento` Para: `Cancelada`
  - De: `Pagamento Aprovado` Para: `Enviado para Transportadora`
  - De: `Pagamento Aprovado` Para: `Cancelada`
  - De: `Enviado para Transportadora`. Para: `Entregue`

## Padrões que foram seguidos no desenvolvimento do projeto

Ao realizar o projeto, utilizei boas práticas de API REST, como Data Transfer Object (DTO), facilitando a leitura e escrita dos dados e o uso correto dos verbos HTTP.  
Na resolução do projeto foram seguidas outras boas práticas de programação, uma muito utilizada foi o TDD (Test Driven Development), dessa forma o código foi programado partindo de testes, garantindo que quando implementado, funcionará como o esperado.  
SOLID também foi utilizado, através do princípio da responsabilidade única, os métodos possuem funções específicas, assim como as classes, outro princípio utilizado foi a inversão de dependência, com isso nosso código não depende de implementações, ele depende de abstrações (nesse caso, interfaces), para isso utilizou-se a injeção de dependência e inversão de controle, melhorando a qualidade dos acoplamentos do código.  
Também por conta do último princípio, foi possível manter nosso código fechado para mudanças e aberto para novidades, para isso pode-se usar o padrão decorator, ao invés de alterarmos a implementação que assina nossa interface utilizada, podemos criar uma nova que utilizará a principal nas funções comuns, e alterar o código apenas nas novidades. Com isso nosso código aceitará atualizações continuando estável.  
Ainda sobre o SOLID, foram seguidos os princípios de LISKOV (descendentes podem ser substituídos pelos ancestrais, ou seja, se uma implementação assinou uma abstração, ela resolve corretamente todas as suas promessas) e de segregação de interface (para continuar resolvendo corretamente as promessas, deve-se separar as abstrações quando necessário).  
O Projeto foi estruturado seguindo o DDD (Domain Driven Design), dessa forma ele foi separado em cinco camadas, sendo elas: domínio, infraestrutura, aplicação, apresentação e testes.

### Arquitetura do projeto
![Arquitetura do projeto](Imagens/arquitetura_api.png)

A camada domínio contém os principais arquivos do projeto, os que serão utilizados ao longo das outras camadas, sendo eles: entidades (venda e vendedor), exceções, as models (enum de estatus e outra classe suporte responsável por armazenar as transições válidas de status), algumas interfaces e o serviço.  
A camada infraestrutura depende da camada de domínio, ela normalmente é responsável por se comunicar com o banco de dados, realizando as inserções e buscas de dados, como eles estão sendo salvos em memória nesse projeto, ela realiza a persistência deles, validando os dados, atribuindo os identificadores únicos e tendo o serviço.  
A camada de aplicação depende da de domínio, nela temos os DTOs de venda e vendedor, facilitando sua leitura e escrita, os profiles (conversão entre o DTO e a entidade), interfaces e aplicação do serviço.  
A camada de apresentação utiliza as três camadas anteriores, nela temos nosso código responsável pela API, utilizando a documentação via swagger e sua controller.  
A última camada é a de testes, ela depende das camadas de infraestrutura, aplicação e apresentação, nela se encontram os testes automatizados, os quais garantem o bom funcionamento do programa.  

## Ferramentas utilizadas
 - AutoMapper: usado para fazer o mapeamento entre as interfaces e as DTOs.
 - Swashbuckle: usado para permitir a passagem das informações da documentação do código para o swagger.
 - xUnit: usado para criar os testes automatizados.
 - RegularExpressions: usado para criar regex, permitindo a validação de telefone e cpf.

## Diagrama de classe das entidades

![Diagrama Entidades](Imagens/diagrama_entidades_api.png)

Diagrama de classe das DTOs:  
![Diagrama DTOs](Imagens/diagrama_classe_dto_api.png)

Veja como a DTO é bem mais objetiva que nossa classe, pedindo ou mostrando apenas o necessário.  

## Métodos criados
![Métodos Swagger](Imagens/metodos_api.png)

**Endpoints**

| Verbo  | Endpoint                         | Parâmetro | Body               |
|--------|----------------------------------|-----------|--------------------|
| GET    | /Venda/{id}                      | id        | N/A                |
| POST   | /Venda                           | id        | Schema VendaDTOSet |
| PATCH  | /Venda/AtualizarStatusVenda/{id} | id        | N/A                |

Esse é o schema (model) de VendaDTOSet, utilizado para passar para os métodos que o exigirem

```json
{
  "vendedor": {
    "nome": "string",
    "cpf": "string",
    "telefone": "string",
    "email": "string"
  },
  "itensVendidos": [
    "string"
  ]
}
```

## Funcionamento da API
### Testes automatizados
![Resultado dos Testes Automatizados da API](Imagens/resultado_testes_automatizados_api.png)  
Perceba que todos os 38 testes passaram corretamente, levando um tempo médio de 564 ms para serem executados.

### Casos de sucesso
Venda sendo cadastrada com sucesso:  
![Sucesso no Cadastro de Venda](Imagens/sucesso_cadastro_venda.png)  

Sucesso busca venda pelo Id:  
![Sucesso na Busca de Venda](Imagens/sucesso_busca_venda.png)  

Sucesso atualização status da venda:  
![Sucesso na Atualização de Venda](Imagens/sucesso_atualizacao_venda.png)  

### Casos de fracasso
Fracasso na busca de uma venda não existente:  
![Fracasso na Busca de Venda Não Existente](Imagens/fracasso_busca_venda.png)  

Fracasso no cadastro de uma venda sem nome do vendedor:  
![Fracasso na Criação de Venda Sem o Nome do Vendedor](Imagens/fracasso_cadastro_sem_nome.png)  
Esse erro ocorre para cpf, e-mail, telefone e itens vendidos.

Fracasso no cadastro de uma venda com telefone inválido:  
![Fracasso na Criação de Venda Com o Telefone do Vendedor Inválido](Imagens/fracasso_cadastro_telefone_invalido.png)  

Fracasso no cadastro de uma venda com cpf inválido:  
![Fracasso na Criação de Venda Com o CPF do Vendedor Inválido](Imagens/fracasso_cadastro_cpf_invalido.png)  

Fracasso na cadastro de uma venda com lista vazia:  
![Fracasso na Criação de Venda Com Lista Vazia](Imagens/fracasso_cadastro_lista_vazia.png)  

Fracasso atualização de status com status indevido:  
![Fracasso na Atualização de Status](Imagens/fracasso_atualizacao_status.png)  

Fracasso atualização de status de venda inexistente:  
![Fracasso na Atualização de Status de Venda Não Existente](Imagens/fracasso_atualizacao_status_venda_inexistente.png)  

## Vídeo
Criei um vídeo mostrando o funcionamento do projeto e explicando seu código, ele está presente nesse link:

[![Vídeo Mostrando o Projeto](Imagens/metodos_api.png)](https://youtu.be/k8es_loJlWY)  

Link: https://youtu.be/k8es_loJlWY

## Segundo projeto com banco de dados
Nesse projeto eu persisti os dados em memória (utilizando o Singleton), deixo o link de um outro projeto, o qual terá a mesma funcionalidade que este, mas utilizará o banco de dados SQL Server.

Link do segundo projeto: https://gitlab.com/ViniciusFregati/payment-api-with-database

## Agradecimentos
Muitíssimo obrigado pela oportunidade de aprendizagem, me desenvolvi bastante durante o bootcamp e após ele procurei cursos extras para conseguir aplicar os testes, TDD, SOLID e DDD no projeto.
