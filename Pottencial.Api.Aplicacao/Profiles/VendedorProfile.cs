﻿using AutoMapper;
using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Dominio.Entidades;

namespace Pottencial.Api.Aplicacao.Profiles
{
    /// <summary>
    /// Classe suporte, ela possuí as transições de vendendedor e de seus DTOs.
    /// </summary>
    public class VendedorProfile : Profile
    {
        public VendedorProfile()
        {
            CreateMap<VendedorDTOSet, Vendedor>();
            CreateMap<Vendedor, VendedorDTOGet>();
        }
    }
}
