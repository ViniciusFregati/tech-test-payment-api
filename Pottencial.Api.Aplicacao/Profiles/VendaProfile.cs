﻿using AutoMapper;
using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Dominio.Entidades;

namespace Pottencial.Api.Aplicacao.Profiles
{
    /// <summary>
    /// Classe suporte, ela possuí as transições da venda e de suas DTOs.
    /// </summary>
    public class VendaProfile : Profile
    {
        public VendaProfile()
        {
            CreateMap<VendaDTOSet, Venda>();
            CreateMap<Venda, VendaDTOGet>();
        }
    }
}
