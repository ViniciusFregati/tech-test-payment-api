﻿using Pottencial.Api.Dominio.Models;

namespace Pottencial.Api.Aplicacao.DTOs
{
    /// <summary>
    /// Data transfer object de leitura da venda, ele possuí apenas as informações
    /// necessárias a serem exibidas como resposta de uma busca.
    /// </summary>
    public class VendaDTOGet
    {
        public EnumStatusVenda StatusVenda { get; set; }
        public int VendedorId { get; set; }
        public VendedorDTOGet Vendedor { get; set; }
        public DateTime Data { get; set; }
        public List<string> ItensVendidos { get; set; }
    }
}
