﻿using Pottencial.Api.Aplicacao.DTOs;
using Pottencial.Api.Dominio.Entidades;
using Pottencial.Api.Dominio.Models;

namespace Pottencial.Api.Aplicacao.Interfaces
{
    public interface IVendaServicoApp
    {
        public Venda RegistrarVenda(VendaDTOSet venda);
        public VendaDTOGet BuscarVendaPeloId(int id);
        public bool AtualizarStatusVenda(int id, EnumStatusVenda novoStatus);
    }
}
